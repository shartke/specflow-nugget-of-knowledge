﻿using NUnit.Framework;
using Specflow.Nugget.Business;
using System;
using TechTalk.SpecFlow;

namespace Specflow.Nugget.Specs
{

	[Binding]
	public class SalesTaxCalculatorSteps
	{
		private SalesTaxCalculator _calculator;
		private double _taxAmount;

		[Given(@"I have entered (.*) as the receipt amount and (.*) as the County into the sales calculator")]
		public void GivenIHaveEnteredAsTheReceiptAmountAndAsTheCountyIntoTheSalesCalculator(double receiptAmount, County county)
		{
			_calculator = new SalesTaxCalculator(receiptAmount, county);
		}

		[When(@"I press Calculate")]
		public void WhenIPressCalculate()
		{
			_taxAmount = _calculator.CalculateSalesTaxByCounty();
		}

		[Then(@"the result should be (.*)")]
		public void ThenTheResultShouldBe(Decimal expectedAmount)
		{
			Assert.AreEqual(expectedAmount, _taxAmount);
		}
	}
}
