﻿Feature: UserProfile
	In order to manage my information
	As a user logged into the site
	I want to see my profile details

@mytag
Scenario: Should see password link on the manage profile page
	Given I login to the site with the username 'shawn.hartke@live.com' and the password 'password'
	When I click the Manage profile link
	Then I should see a reset password link with the text 'Change your password'
