﻿Feature: SalesTaxCalculator
	In order to accurately calculate sales tax by county
	As an accountant
	I want to be shown the sales tax based on county and receipt amount


Scenario: Calculate Sales tax for Tooele
	Given I have entered 50 as the receipt amount and Tooele as the County into the sales calculator
	When I press Calculate
	Then the result should be 3.3