﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Specflow.Nugget.Business
{
	public class SalesTaxCalculator
	{
		private double _amount;
		private County _county;

		public SalesTaxCalculator(double amount, County county)
		{
			_amount = amount;
			_county = county;
		}

		public double CalculateSalesTaxByCounty()
		{
			double salesTax = 0.0;

			switch (_county)
			{
				case County.Utah:
					salesTax = 6.85;
					break;
				case County.Tooele:
					salesTax = 6.6;
					break;
				case County.Weber:
					salesTax = 7.3;
					break;
			}

			return _amount * salesTax /100;
		}
	}
}

public enum County
{
	Utah,
	Weber,
	Tooele
}

