﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Specflow.Nugget.Web.Startup))]
namespace Specflow.Nugget.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
